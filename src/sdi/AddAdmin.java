package sdi;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SwingConstants;

import com.mysql.jdbc.PreparedStatement;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AddAdmin extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JTextField name;
	private JPasswordField password;
	private JTextField job;
	private JTextField id;
	private JButton addButton;
	
	public static String encryptPassword(String password) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddAdmin frame = new AddAdmin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddAdmin() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 560, 403);
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icone.png")));
		contentPane = new JPanel();
		contentPane.setBackground(new Color(43, 45, 49));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(242, 83, 91));
		panel.setBounds(10, 0, 524, 65);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("CAR REGISTER");
		lblNewLabel.setFont(new Font("Segoe Script", Font.BOLD, 40));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBackground(new Color(0, 0, 255));
		panel.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setBorder(null);
		btnNewButton.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton.setIcon(new ImageIcon(AddAdmin.class.getResource("/images/previous.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddAdmin.this.dispose();
				Administrator admin = new Administrator();
				admin.setVisible(true);
				admin.setLocationRelativeTo(null);
				
			}
		});
		
		lblNewLabel_1 = new JLabel("Nom :");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 121, 50, 22);
		contentPane.add(lblNewLabel_1);
		
		name = new JTextField();
		name.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 11));
		name.setBounds(100, 121, 329, 22);
		contentPane.add(name);
		name.setColumns(10);
		lblNewLabel_2 = new JLabel("Poste :");
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		lblNewLabel_2.setBounds(40, 165, 86, 22);
		contentPane.add(lblNewLabel_2);
		
		job = new JTextField();
		job.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 11));
		job.setColumns(10);
		job.setBounds(138, 165, 291, 22);
		contentPane.add(job);
		
		lblNewLabel_3 = new JLabel("Nom d'utilisateur :");
		lblNewLabel_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_3.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		lblNewLabel_3.setBounds(40, 209, 160, 22);
		contentPane.add(lblNewLabel_3);
		
		id = new JTextField();
		id.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 11));
		id.setColumns(10);
		id.setBounds(210, 209, 219, 22);
		contentPane.add(id);
		
		lblNewLabel_4 = new JLabel("Mot de passe :");
		lblNewLabel_4.setForeground(new Color(255, 255, 255));
		lblNewLabel_4.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		lblNewLabel_4.setBounds(40, 253, 120, 22);
		contentPane.add(lblNewLabel_4);		
		
		password = new JPasswordField();
		password.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 11));
		password.setBounds(170, 253, 259, 22);
		contentPane.add(password);
		
		addButton = new JButton("Ajouter");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(name.getText().equals("") ||  job.getText().equals("") || id.getText().equals("") || password.getPassword().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Veuillez remplir tous les champs");
				}
				else
				{
					Connection conn;
					try {
						conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");
						conn.createStatement();
						char[] p = password.getPassword();
						String pass = new String(p);				    
						
						PreparedStatement ps = (PreparedStatement) conn.prepareStatement("INSERT INTO admin (username, password, name, situation) VALUES (?, ?, ?, ?)");
						ps.setString(1, id.getText());
						ps.setString(2, encryptPassword(pass));
						ps.setString(3, name.getText());
						ps.setString(4, job.getText());
						ps.executeUpdate();

						JOptionPane.showInternalMessageDialog(null, "Administrateur ajout� avec succ�s");
						
						id.setText("");
						password.setText("");
						name.setText("");
						job.setText("");
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
				
			}
			
		});
		addButton.setForeground(new Color(255, 255, 255));
		addButton.setBackground(new Color(242, 83, 91));
		addButton.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		addButton.setBounds(40, 298, 100, 23);
		contentPane.add(addButton);
		
		
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNewButton.setBackground(new Color(43, 45, 49));
		btnNewButton.setBounds(10, 76, 50, 23);
		contentPane.add(btnNewButton);
		
	}
}
