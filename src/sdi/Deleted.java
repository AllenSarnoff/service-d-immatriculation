package sdi;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Deleted extends JFrame {

	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Deleted frame = new Deleted();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Deleted() {
		setSize(350, 350);
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icone.png")));
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		
		Icon imgIcon = new ImageIcon(this.getClass().getResource("../images/deleted.gif"));
		JLabel label = new JLabel(imgIcon);
		label.setSize(400, 400);
		this.getContentPane().add(label);
		this.setLocationRelativeTo(null);
		
		CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS)
	    .execute(() -> {
	        Deleted.this.dispose();// Code � ex�cuter apr�s le d�lai sp�cifi�
	    });
	}

}
