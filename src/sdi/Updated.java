package sdi;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Updated extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Updated frame = new Updated();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Updated() {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(310, 310);
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icone.png")));
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		
		Icon imgIcon = new ImageIcon(this.getClass().getResource("../images/done.gif"));
		JLabel label = new JLabel(imgIcon);
		label.setBounds(0, 0, 400, 400);
		this.getContentPane().add(label);
		this.setLocationRelativeTo(null);
		
		CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS)
	    .execute(() -> {
	        Updated.this.dispose();// Code � ex�cuter apr�s le d�lai sp�cifi�
	    });
	}

}