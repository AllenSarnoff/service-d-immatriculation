package sdi;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.SwingConstants;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class TableAdmin extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JTextField nom;
	private JPasswordField mdp;
	private JTextField fonction;
	private JTextField identifiant;
	private JButton deleteButton;
	private JTable table;

	public static String encryptPassword(String password) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }
	
	public void tab() {		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");
			Statement stt = (Statement) conn.createStatement();
			ResultSet rs = stt.executeQuery("SELECT * FROM admin");
			DefaultTableModel dtbm = (DefaultTableModel) table.getModel();
			
			String[] colName = {"Nom", "Poste", "Nom d'utilisateur", "Mot de passe"};
				dtbm.setColumnIdentifiers(colName);
				
			while(rs.next())
				{
					String name, function, username, password;
					name = rs.getString(3);
					function = rs.getString(4);
					username = rs.getString(1);
					password = rs.getString(2);
					
					String[] row = {name, function, username, password};
					dtbm.addRow(row);
				}
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

	/**
	 * Create the frame.
	 */
	public TableAdmin() {
		
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 659, 544);
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icone.png")));
		contentPane = new JPanel();
		contentPane.setBackground(new Color(43, 45, 49));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		table = new JTable();
		scrollPane.setViewportView(table);
		
		
		DefaultTableModel dtb = (DefaultTableModel) table.getModel();
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int k=table.getSelectedRow();
				nom.setText(dtb.getValueAt(k, 0).toString());
				fonction.setText(dtb.getValueAt(k, 1).toString());
				identifiant.setText(dtb.getValueAt(k, 2).toString());
				mdp.setText(dtb.getValueAt(k, 3).toString());
			}
		});
		
		scrollPane.setBounds(10, 338, 623, 123);
		contentPane.add(scrollPane);
		
		tab();
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(242, 83, 91));
		panel.setBounds(10, 0, 623, 65);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("CAR REGISTER");
		lblNewLabel.setFont(new Font("Segoe Script", Font.BOLD, 40));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBackground(new Color(0, 0, 255));
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Nom :");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		lblNewLabel_1.setBounds(40, 121, 50, 22);
		contentPane.add(lblNewLabel_1);
		
		nom = new JTextField();
		nom.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 11));
		nom.setBounds(100, 121, 408, 22);
		contentPane.add(nom);
		nom.setColumns(10);
		
		lblNewLabel_2 = new JLabel("Fonction :");
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		lblNewLabel_2.setBounds(40, 165, 86, 22);
		contentPane.add(lblNewLabel_2);
		
		fonction = new JTextField();
		fonction.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 11));
		fonction.setColumns(10);
		fonction.setBounds(138, 165, 370, 22);
		contentPane.add(fonction);
		
		lblNewLabel_3 = new JLabel("Nom d'utilisateur :");
		lblNewLabel_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_3.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		lblNewLabel_3.setBounds(40, 209, 160, 22);
		contentPane.add(lblNewLabel_3);
		
		identifiant = new JTextField();
		identifiant.setEditable(false);
		identifiant.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 11));
		identifiant.setColumns(10);
		identifiant.setBounds(210, 209, 298, 22);
		contentPane.add(identifiant);
		
		lblNewLabel_4 = new JLabel("Mot de passe :");
		lblNewLabel_4.setForeground(new Color(255, 255, 255));
		lblNewLabel_4.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		lblNewLabel_4.setBounds(40, 253, 120, 22);
		contentPane.add(lblNewLabel_4);		
		
		mdp = new JPasswordField();
		mdp.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 11));
		mdp.setBounds(170, 253, 338, 22);
		contentPane.add(mdp);
		
		
		JButton updateButton = new JButton("Modifier");
		updateButton.setHorizontalTextPosition(SwingConstants.LEADING);
		updateButton.setIcon(new ImageIcon(TableAdmin.class.getResource("/images/edit.png")));
		updateButton.setHorizontalAlignment(SwingConstants.LEFT);
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(nom.getText().equals("") ||  fonction.getText().equals("") || identifiant.getText().equals("") || mdp.getPassword().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Veuillez selectionner une ligne");
				}
				else
				{
					Connection conn;
					try {
						conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");
						char[] p = mdp.getPassword();
						String pass = new String(p);
						
						PreparedStatement ps = (PreparedStatement) conn.prepareStatement("UPDATE admin SET password=?, name=?, situation=? WHERE username=?");
						ps.setString(1, encryptPassword(pass));
						ps.setString(2, nom.getText());
						ps.setString(3, fonction.getText());
						ps.setString(4, identifiant.getText());
						ps.executeUpdate();

						TableAdmin.this.dispose();
						
						Updated upd = new Updated();
				        upd.setVisible(true);
						
						
						
						CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS)
					    .execute(() -> {
									
									  TableAdmin t_a = new TableAdmin(); 
									  t_a.setVisible(true);
									  t_a.setLocationRelativeTo(null);
									  
									  
									 
					    });
						
												
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			}
		});
		updateButton.setForeground(new Color(242, 83, 91));
		updateButton.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		updateButton.setBackground(new Color(43, 45, 49));
		updateButton.setBounds(44, 298, 154, 32);
		contentPane.add(updateButton);
		
		deleteButton = new JButton("Supprimer");
		deleteButton.setHorizontalTextPosition(SwingConstants.LEADING);
		deleteButton.setIcon(new ImageIcon(TableAdmin.class.getResource("/images/delete.png")));
		deleteButton.setHorizontalAlignment(SwingConstants.LEFT);
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			

					if(nom.getText().equals("") ||  fonction.getText().equals("") || identifiant.getText().equals("") || mdp.getPassword().equals(""))
					{
						JOptionPane.showMessageDialog(null, "Veuillez selectionner une ligne");
					}
					else {					
						/*int confirm = JOptionPane.showMessageDialog(null, "Voulez-vous supprimer cet administrateur?", "Confirmation de suppression", JOptionPane.YES_NO_OPTION);
						if(confirm)
						{  }*/
							Connection conn;
						try {
							conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");
							PreparedStatement ps = (PreparedStatement) conn.prepareStatement("DELETE FROM admin WHERE username=?");
							
							ps.setString(1, identifiant.getText());
							ps.executeUpdate();
							
							TableAdmin.this.dispose();
							
							Deleted dlt = new Deleted();
							dlt.setVisible(true);
							
							
							
							CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS)
						    .execute(() -> {
										
										  /*TableAdmin t_a = new TableAdmin(); 
										  t_a.setVisible(true);
										  t_a.setLocationRelativeTo(null);*/
						    	
										 
						    });
						
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}		
					}
				
		});
		deleteButton.setForeground(new Color(242, 83, 91));
		deleteButton.setBackground(new Color(43, 45, 49));
		deleteButton.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		deleteButton.setBounds(444, 298, 158, 32);
		contentPane.add(deleteButton);
		
		
		
		JButton btnNewButton = new JButton(" ");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					TableAdmin.this.dispose();
					Administrator admin = new Administrator();
					admin.setVisible(true);
			}
		});
		btnNewButton.setBorder(null);
		btnNewButton.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton.setIcon(new ImageIcon(TableAdmin.class.getResource("/images/previous.png")));
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNewButton.setBackground(new Color(43, 45, 49));
		btnNewButton.setBounds(10, 76, 48, 23);
		contentPane.add(btnNewButton);
		
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TableAdmin frame = new TableAdmin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}