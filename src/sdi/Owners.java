package sdi;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
//import java.awt.event.KeyAdapter;
//import java.awt.event.KeyEvent;

public class Owners extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private JScrollPane scrollPane;
	private JLabel lblNewLabel_2;
	private JTextField textField_1;
	private JLabel lblNewLabel_2_1;
	private JLabel lblNewLabel_2_2;
	private JTextField textField_2;
	private JLabel lblNewLabel_2_3;
	private JLabel lblNewLabel_1;
	private JTextField textField_3;
	private JLabel lblNewLabel_2_4;
	private JTextField textField_4;
	private JLabel lblNewLabel_2_5;
	private JTextField textField_5;
	private JLabel lblNewLabel_2_6;
	private JTextField textField_6;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JTextField sexe;

	public void tbl() {		
		try {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");
		Statement stt = (Statement) conn.createStatement();
		ResultSet rs = stt.executeQuery("SELECT * FROM owners");
		DefaultTableModel dtbm = (DefaultTableModel) table.getModel();
		
		String[] colName = {"Nom", "Sexe", "CIN", "Adresse", "T�l�phone", "Travail", "Lieu de travail"};
			dtbm.setColumnIdentifiers(colName);
			
		while(rs.next())
			{
				String full_name, sex, CIN, address, phone, job, job_location;
				full_name = rs.getString(1); 
				sex = rs.getString(2); 
				CIN = rs.getString(3); 
				address = rs.getString(4); 
				phone = rs.getString(5);
				job = rs.getString(6);
				job_location = rs.getString(7);
				String[] row = {full_name ,sex ,CIN ,address, phone, job, job_location};
				dtbm.addRow(row);
			}
		
	} catch (ClassNotFoundException | SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
	 
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Owners frame = new Owners();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Owners() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(900, 700);
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icone.png")));
		contentPane = new JPanel();
		contentPane.setForeground(new Color(255, 255, 255));
		contentPane.setBackground(new Color(43, 45, 49));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(242, 83, 91));
		panel.setBounds(10, 0, 864, 65);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("CAR REGISTER");
		lblNewLabel.setFont(new Font("Segoe Script", Font.BOLD, 40));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBackground(new Color(0, 0, 255));
		panel.add(lblNewLabel);
		
		lblNewLabel_2 = new JLabel("Nom et pr\u00E9noms");
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2.setBounds(20, 121, 147, 19);
		contentPane.add(lblNewLabel_2);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		textField_1.setColumns(10);
		textField_1.setBounds(177, 120, 237, 20);
		contentPane.add(textField_1);
		
		lblNewLabel_2_1 = new JLabel("Sexe");
		lblNewLabel_2_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_1.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_1.setBounds(20, 156, 147, 14);
		contentPane.add(lblNewLabel_2_1);
		
		sexe = new JTextField();
		sexe.setBounds(177, 154, 237, 20);
		contentPane.add(sexe);
		sexe.setColumns(10);
		
		lblNewLabel_2_2 = new JLabel("CIN");
		lblNewLabel_2_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_2.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_2.setBounds(20, 191, 147, 14);
		contentPane.add(lblNewLabel_2_2);
		
		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		textField_2.setColumns(10);
		textField_2.setBounds(177, 189, 237, 20);
		contentPane.add(textField_2);
		
		lblNewLabel_2_3 = new JLabel("Num\u00E9ro de t\u00E9l\u00E9phone");
		lblNewLabel_2_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_3.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_3.setBounds(20, 226, 147, 14);
		contentPane.add(lblNewLabel_2_3);
		
		lblNewLabel_1 = new JLabel("+261");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(177, 227, 56, 14);
		contentPane.add(lblNewLabel_1);
		
		textField_3 = new JTextField();
		textField_3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		textField_3.setColumns(10);
		textField_3.setBounds(212, 224, 202, 20);
		contentPane.add(textField_3);
		
		lblNewLabel_2_4 = new JLabel("Adresse");
		lblNewLabel_2_4.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_4.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_4.setBounds(20, 260, 147, 14);
		contentPane.add(lblNewLabel_2_4);
		
		textField_4 = new JTextField();
		textField_4.setFont(new Font("Tahoma", Font.PLAIN, 13));
		textField_4.setColumns(10);
		textField_4.setBounds(177, 258, 237, 20);
		contentPane.add(textField_4);
		
		lblNewLabel_2_5 = new JLabel("Travail");
		lblNewLabel_2_5.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_5.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_5.setBounds(20, 295, 147, 14);
		contentPane.add(lblNewLabel_2_5);
		
		textField_5 = new JTextField();
		textField_5.setFont(new Font("Tahoma", Font.PLAIN, 13));
		textField_5.setColumns(10);
		textField_5.setBounds(177, 293, 237, 20);
		contentPane.add(textField_5);
		
		lblNewLabel_2_6 = new JLabel("Lieu de travail");
		lblNewLabel_2_6.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_6.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_6.setBounds(20, 330, 147, 14);
		contentPane.add(lblNewLabel_2_6);
		
		textField_6 = new JTextField();
		textField_6.setFont(new Font("Tahoma", Font.PLAIN, 13));
		textField_6.setColumns(10);
		textField_6.setBounds(177, 328, 237, 20);
		contentPane.add(textField_6);
		
		btnNewButton_1 = new JButton("Modifier");
		btnNewButton_1.setBorder(null);
		btnNewButton_1.setHorizontalTextPosition(SwingConstants.LEFT);
		btnNewButton_1.setIcon(new ImageIcon(Owners.class.getResource("/images/edit.png")));
		btnNewButton_1.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField_1.getText().equals("") ||  sexe.getText().equals("") || textField_3.getText().equals("") || textField_4.getText().equals("") || textField_5.getText().equals("") || textField_6.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Veuillez selectionner une ligne \nEt assurez-vous de remplir tous les champs");
				}
				else
				{
					Connection conn ;
					try {
						conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");
						PreparedStatement ps = (PreparedStatement) conn.prepareStatement("UPDATE owners SET full_name=?, sex=?, address=?, phone=?, job=?, job_location=? WHERE CIN=?");
						ps.setString(1, textField_1.getText());
						ps.setString(2, sexe.getText());
						ps.setString(3, textField_4.getText());
						ps.setString(4, textField_3.getText());
						ps.setString(5, textField_5.getText());
						ps.setString(6, textField_6.getText());
						ps.setString(7, textField_2.getText());
						ps.executeUpdate();
						
						PreparedStatement ps1 = (PreparedStatement) conn.prepareStatement("UPDATE certificate SET address=?, job=?, sex=? WHERE full_name=?");
						ps1.setString(1, textField_4.getText());
						ps1.setString(2, textField_5.getText());
						ps1.setString(3, sexe.getText());
						ps1.setString(4, textField_1.getText());
						ps1.executeUpdate();

						Owners.this.dispose();
						
						Updated upd = new Updated();
				        upd.setVisible(true);
						
						
						
						CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS)
					    .execute(() -> {
									
									  Owners ow = new Owners(); 
									  ow.setVisible(true);
									  ow.setLocationRelativeTo(null);
									 
					    });
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			}
		});
		btnNewButton_1.setForeground(new Color(242, 83, 91));
		btnNewButton_1.setBackground(new Color(43, 45, 49));
		btnNewButton_1.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		btnNewButton_1.setBounds(10, 366, 99, 32);
		contentPane.add(btnNewButton_1);
		
		btnNewButton_2 = new JButton("Supprimer");
		btnNewButton_2.setBorder(null);
		btnNewButton_2.setIcon(new ImageIcon(Owners.class.getResource("/images/delete.png")));
		btnNewButton_2.setHorizontalTextPosition(SwingConstants.LEADING);
		btnNewButton_2.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField_1.getText().equals("") ||  sexe.getText().equals("") || textField_3.getText().equals("") || textField_4.getText().equals("") || textField_5.getText().equals("") || textField_6.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Veuillez selectionner une ligne");
				}
				else
				{
						Connection conn;
					try {
						conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");
						PreparedStatement ps = (PreparedStatement) conn.prepareStatement("DELETE FROM owners WHERE CIN=?");
						ps.setString(1, textField_2.getText());
						ps.executeUpdate();											
						
						Owners.this.dispose();

						Deleted dlt = new Deleted();
						dlt.setVisible(true);
						
												
						CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS)
					    .execute(() -> {
									
					    	Owners ow = new Owners(); 
					    	ow.setVisible(true);
					    	ow.setLocationRelativeTo(null);

					    });
						
					
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				}
					
				}
		});
		btnNewButton_2.setForeground(new Color(242, 83, 91));
		btnNewButton_2.setBackground(new Color(43, 45, 49));
		btnNewButton_2.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		btnNewButton_2.setBounds(310, 366, 111, 32);
		contentPane.add(btnNewButton_2);
		
		scrollPane = new JScrollPane();
		table = new JTable();
		scrollPane.setViewportView(table);
		
		tbl();
		
		DefaultTableModel dtb = (DefaultTableModel) table.getModel();
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int k=table.getSelectedRow();
				textField_1.setText(dtb.getValueAt(k, 0).toString());
				sexe.setText(dtb.getValueAt(k, 1).toString());
				textField_2.setText(dtb.getValueAt(k, 2).toString());
				textField_3.setText(dtb.getValueAt(k, 4).toString());
				textField_4.setText(dtb.getValueAt(k, 3).toString());
				textField_5.setText(dtb.getValueAt(k, 5).toString());
				textField_6.setText(dtb.getValueAt(k, 6).toString());
			}
		});
		
		scrollPane.setBounds(10, 420, 864, 220);
		contentPane.add(scrollPane);		
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Owners.this.dispose();
				Administrator admin = new Administrator();
				admin.setVisible(true);
			}
		});
		btnNewButton_3.setIcon(new ImageIcon(Owners.class.getResource("/images/previous.png")));
		btnNewButton_3.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_3.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNewButton_3.setBorder(null);
		btnNewButton_3.setBackground(new Color(43, 45, 49));
		btnNewButton_3.setBounds(10, 76, 50, 23);
		contentPane.add(btnNewButton_3);
		
	}
}
