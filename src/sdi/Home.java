package sdi;

import java.awt.*;
import javax.swing.*;
import com.mysql.jdbc.Statement;

import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.border.MatteBorder;



public class Home extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPasswordField password;
	
	public static String encryptPassword(String password) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(password.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

	public Home() {
		getContentPane().setBackground(new Color(33, 33, 33));
		getContentPane().setForeground(new Color(255, 255, 255));
		setResizable(false);
		setTitle("logIn page");
		setSize (497, 535) ;
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icone.png")));
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel_2 = new JLabel("BIENVENUE");
		lblNewLabel_2.setBounds(135, 90, 220, 28);
		lblNewLabel_2.setForeground(new Color(209, 31, 45));
		lblNewLabel_2.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 34));
		lblNewLabel_2.setBackground(new Color(33, 33, 33));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel = new JLabel("Login :");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Segoe Print", Font.BOLD, 15));
		lblNewLabel.setBounds(137, 339, 64, 22);
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		getContentPane().add(lblNewLabel);
		
		JTextArea username = new JTextArea();
		username.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
		username.setForeground(new Color(255, 255, 255));
		username.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(255, 255, 255)));
		username.setFont(new Font("Segoe Print", Font.PLAIN, 15));
		username.setBounds(229, 339, 120, 22);
        username.setOpaque(false);
		getContentPane().add(username);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setForeground(new Color(255, 255, 255));
		lblPassword.setFont(new Font("Segoe Print", Font.BOLD, 15));
		lblPassword.setBounds(137, 372, 96, 22);
		lblPassword.setHorizontalAlignment(SwingConstants.LEFT);
		getContentPane().add(lblPassword);
		
		password = new JPasswordField();
		password.setForeground(new Color(255, 255, 255));
		password.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(255, 255, 255)));
		password.setOpaque(false);
		password.setFont(new Font("Segoe Print", Font.PLAIN, 15));
		password.setBounds(229, 372, 120, 22);
		getContentPane().add(password);
	
		JButton btnNewButton_1 = new JButton("Connexion");
		btnNewButton_1.setBorder(null);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(username.getText().equals("") || password.getPassword().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Veuillez remplir tous les champs");
				}
				else
				{
					String user = username.getText();
					char[] p = password.getPassword();
					String pass = new String(p);
					String passwordHashed = encryptPassword(pass);
					
					java.sql.Connection conn;
					try {
						conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");
						Statement stt = (Statement) conn.createStatement();
						ResultSet rs = stt.executeQuery("SELECT * FROM admin WHERE username = \""+user+" \" AND password = \""+passwordHashed+"\"");
						
						
						if(!rs.next())
						{
							JOptionPane.showInternalMessageDialog(null, "Cet administrateur n'existe pas dans la base de donn�es\nV�rifiez les informations que vous avez saisies");
						}
						else
						{
							
							Administrator admin = new Administrator();
							
							admin.setVisible(true);
							admin.setLocationRelativeTo(null);
							Home.this.dispose();
													
						}
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					

				}
				
			}
		});
		btnNewButton_1.setBounds(164, 411, 141, 34);
		btnNewButton_1.setForeground(new Color(209, 31, 45));
		btnNewButton_1.setBackground(new Color(33, 33, 33));
		btnNewButton_1.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 20));
		getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Home.class.getResource("/images/Screenshot 2023-03-20 at 18-13-18 Simple Outline and Car Logo DesignEvo Logo Maker.png")));
		lblNewLabel_1.setBounds(0, 0, 444, 433);
		getContentPane().add(lblNewLabel_1);
		
	}
	
	public static void main(String[] args) {
		Home form = new Home();
		form.setVisible(true);
		}
}
	