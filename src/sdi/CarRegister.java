package sdi;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;


public class CarRegister extends JFrame {

	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField num;
	private JTextField marque;
	private JTextField couleur;
	private JTextField usage;
	private JTextField date_imm;
	private JTextField nom;
	private JTextField cin;
	private JTextField telephone;
	private JTextField adresse;
	private JTextField profession;
	private JTextField lieu_travail;
	private JTextField sexe;
	private JTable table;
	
	public void ta(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/CAR_REGISTER_ANTANANARIVO", "root", "");
			Statement stt = (Statement) conn.createStatement();
			ResultSet rs = stt.executeQuery("SELECT * FROM certificate");
			DefaultTableModel dtbm = (DefaultTableModel) table.getModel();
			
			String[] colName = {"Num�ro d'immatriculation", "Propri�taire","Adresse", "Profession","Sexe", "Marque", "Date d'immatriculation"};
				dtbm.setColumnIdentifiers(colName); 
				
			while(rs.next())
				{
					String num_car, name,adresse,profession,sexe, mark, register_date;
					num_car = rs.getString(1); 
					name = rs.getString(2);
					adresse = rs.getString(3);
					profession = rs.getString(4);
					sexe = rs.getString(5);
					mark = rs.getString(6); 
					register_date = rs.getString(7);
					String[] row = {num_car, name, adresse,profession ,sexe ,mark, register_date};
					dtbm.addRow(row);
				}
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CarRegister frame = new CarRegister();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CarRegister() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(851, 645);
		setLocationRelativeTo(null);
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icone.png")));
		contentPane = new JPanel();
		contentPane.setBackground(new Color(43, 45, 49));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(242, 83, 91));
		panel.setBounds(10, 0, 815, 65);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("CAR REGISTER");
		lblNewLabel.setFont(new Font("Segoe Script", Font.BOLD, 40));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBackground(new Color(0, 0, 255));
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Voiture\r\n");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(176, 121, 79, 18);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(60, 62, 70));
		panel_1.setBounds(10, 150, 391, 272);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		contentPane.add(lblNewLabel_1);
		JLabel lblNewLabel_2 = new JLabel("Num\u00E9ro d'immatriculation :");
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2.setBounds(10, 23, 196, 14);
		panel_1.add(lblNewLabel_2);
		
		num = new JTextField();
		num.setFont(new Font("Tahoma", Font.PLAIN, 13));
		num.setBounds(227, 21, 154, 20);
		panel_1.add(num);
		num.setColumns(10);
		
		JLabel lblNewLabel_2_1 = new JLabel("Marque :");
		lblNewLabel_2_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_1.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_1.setBounds(10, 56, 196, 14);
		panel_1.add(lblNewLabel_2_1);
		
		marque = new JTextField();
		marque.setFont(new Font("Tahoma", Font.PLAIN, 13));
		marque.setColumns(10);
		marque.setBounds(227, 54, 154, 20);
		panel_1.add(marque);
		
		JLabel lblNewLabel_2_2 = new JLabel("Couleur :");
		lblNewLabel_2_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_2.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_2.setBounds(10, 92, 196, 14);
		panel_1.add(lblNewLabel_2_2);
		
		couleur = new JTextField();
		couleur.setFont(new Font("Tahoma", Font.PLAIN, 13));
		couleur.setColumns(10);
		couleur.setBounds(227, 90, 154, 20);
		panel_1.add(couleur);
		
		JLabel lblNewLabel_2_3 = new JLabel("Usage :");
		lblNewLabel_2_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_3.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_3.setBounds(10, 126, 196, 17);
		panel_1.add(lblNewLabel_2_3);
		
		usage = new JTextField();
		usage.setFont(new Font("Tahoma", Font.PLAIN, 13));
		usage.setColumns(10);
		usage.setBounds(227, 125, 154, 20);
		panel_1.add(usage);
		
		JLabel lblNewLabel_2_4 = new JLabel("Date d'immatriculation :");
		lblNewLabel_2_4.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_4.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_4.setBounds(10, 163, 196, 14);
		panel_1.add(lblNewLabel_2_4);
		
		date_imm = new JTextField();
		date_imm.setFont(new Font("Tahoma", Font.PLAIN, 13));
		date_imm.setColumns(10);
		date_imm.setBounds(227, 161, 154, 20);
		panel_1.add(date_imm);
		
		JLabel lblNewLabel_1_1 = new JLabel("Propri\u00E9taire");
		lblNewLabel_1_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1_1.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 15));
		lblNewLabel_1_1.setBounds(554, 121, 99, 18);
		contentPane.add(lblNewLabel_1_1);
		
		JPanel panel_1_1 = new JPanel();
		panel_1_1.setBackground(new Color(60, 62, 70));
		panel_1_1.setBounds(411, 150, 414, 272);
		contentPane.add(panel_1_1);
		panel_1_1.setLayout(null);
		
		JLabel lblNewLabel_2_5 = new JLabel("Nom et pr\u00E9noms :");
		lblNewLabel_2_5.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_5.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_5.setBounds(10, 22, 148, 14);
		panel_1_1.add(lblNewLabel_2_5);
		
		nom = new JTextField();
		nom.setFont(new Font("Tahoma", Font.PLAIN, 13));
		nom.setColumns(10);
		nom.setBounds(167, 20, 237, 20);
		panel_1_1.add(nom);
		
		JLabel lblNewLabel_2_6 = new JLabel("Sexe :");
		lblNewLabel_2_6.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_6.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_6.setBounds(10, 56, 148, 14);
		panel_1_1.add(lblNewLabel_2_6);
		
		sexe = new JTextField();
		sexe.setFont(new Font("Tahoma", Font.PLAIN, 13));
		sexe.setColumns(10);
		sexe.setBounds(167, 51, 237, 20);
		panel_1_1.add(sexe);
		
		JLabel lblNewLabel_2_7 = new JLabel("CIN :");
		lblNewLabel_2_7.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_7.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_7.setBounds(10, 91, 148, 14);
		panel_1_1.add(lblNewLabel_2_7);
		
		cin = new JTextField();
		cin.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cin.setColumns(10);
		cin.setBounds(167, 89, 237, 20);
		panel_1_1.add(cin);
		
		JLabel lblNewLabel_2_8 = new JLabel("T�l�phone :");
		lblNewLabel_2_8.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_8.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_8.setBounds(10, 126, 148, 14);
		panel_1_1.add(lblNewLabel_2_8);
		
		JLabel lblNewLabel_3 = new JLabel("+261");
		lblNewLabel_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3.setBounds(170, 127, 36, 14);
		panel_1_1.add(lblNewLabel_3);
		
		telephone = new JTextField();
		telephone.setFont(new Font("Tahoma", Font.PLAIN, 13));
		telephone.setColumns(10);
		telephone.setBounds(202, 124, 202, 20);
		panel_1_1.add(telephone);
		
		JLabel lblNewLabel_2_9 = new JLabel("Adresse :");
		lblNewLabel_2_9.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_9.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_9.setBounds(10, 160, 148, 14);
		panel_1_1.add(lblNewLabel_2_9);
		
		adresse = new JTextField();
		adresse.setFont(new Font("Tahoma", Font.PLAIN, 13));
		adresse.setColumns(10);
		adresse.setBounds(167, 158, 237, 20);
		panel_1_1.add(adresse);
		
		JLabel lblNewLabel_2_10 = new JLabel("Profession :");
		lblNewLabel_2_10.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_10.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_10.setBounds(10, 195, 148, 14);
		panel_1_1.add(lblNewLabel_2_10);
		
		profession = new JTextField();
		profession.setFont(new Font("Tahoma", Font.PLAIN, 13));
		profession.setColumns(10);
		profession.setBounds(167, 193, 237, 20);
		panel_1_1.add(profession);
		
		JLabel lblNewLabel_2_11 = new JLabel("Lieu de travail :");
		lblNewLabel_2_11.setForeground(new Color(255, 255, 255));
		lblNewLabel_2_11.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2_11.setBounds(10, 230, 148, 14);
		panel_1_1.add(lblNewLabel_2_11);
		
		lieu_travail = new JTextField();
		lieu_travail.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lieu_travail.setColumns(10);
		lieu_travail.setBounds(167, 228, 237, 20);
		panel_1_1.add(lieu_travail);
		
		
		
		
		JButton btnNewButton = new JButton("Enregistrer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					if(num.getText().equals("") || marque.getText().equals("") || couleur.getText().equals("") || usage.getText().equals("") || date_imm.getText().equals("") ||
								nom.getText().equals("") || sexe.getText().equals("") || cin.getText().equals("") || telephone.getText().equals("") || 
								adresse.getText().equals("") || profession.getText().equals("") || lieu_travail.getText().equals(""))
						{
							JOptionPane.showMessageDialog(null, "Veuillez remplir tous les champs");
							
						
						}
						else
						{			
							Connection conn;
							try {
								conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");
								
								PreparedStatement ps1 = (PreparedStatement) conn.prepareStatement("INSERT INTO cars (number, mark, color, utilization, register_date) VALUES (?, ?, ?, ?, ?)");
								ps1.setString(1, num.getText());
								ps1.setString(2, marque.getText());
								ps1.setString(3, couleur.getText());
								ps1.setString(4, usage.getText());
								ps1.setString(5, date_imm.getText());
								ps1.executeUpdate();
								
								PreparedStatement ps2 = (PreparedStatement) conn.prepareStatement("INSERT INTO owners (full_name, sex, CIN, address, phone, job, job_location) VALUES (?, ?, ?, ?, ?, ?, ?)");
								ps2.setString(1, nom.getText());
								ps2.setString(2, sexe.getText());
								ps2.setString(3, cin.getText());
								ps2.setString(4, adresse.getText());
								ps2.setString(5, telephone.getText());
								ps2.setString(6, profession.getText());
								ps2.setString(7, lieu_travail.getText());
								ps2.executeUpdate();
								
								PreparedStatement ps3 = (PreparedStatement) conn.prepareStatement("INSERT INTO certificate (number, full_name, address, job, sex, mark, register_date) VALUES (?, ?, ?, ?, ?, ?, ?)");
								ps3.setString(1, num.getText());
								ps3.setString(2, nom.getText());
								ps3.setString(3, adresse.getText());
								ps3.setString(4, profession.getText());
								ps3.setString(5, sexe.getText());
								ps3.setString(6, marque.getText());
								ps3.setString(7, date_imm.getText());
								ps3.executeUpdate();
								
								JOptionPane.showInternalMessageDialog(null, "Ajout� avec succ�s");
								
								//Minik-Bold
								//Canterbury Sans Medium
								//Serif
								//Sans Serif
								
								num.setText("");
								marque.setText("");
								couleur.setText(""); 
								usage.setText(""); 
								date_imm.setText("");
								nom.setText(""); 
								sexe.setText("");
								cin.setText(""); 
								telephone.setText("");
								adresse.setText(""); 
								profession.setText(""); 
								lieu_travail.setText("");
								
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						
						}
					}
				});
			
		btnNewButton.setFont(new Font("Lucida Calligraphy", Font.BOLD, 15));
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(242, 83, 91));
		btnNewButton.setBounds(695, 442, 130, 32);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CarRegister.this.dispose();
				Administrator admin = new Administrator();
				admin.setVisible(true);
			}
		});
		btnNewButton_1.setIcon(new ImageIcon(CarRegister.class.getResource("/images/previous.png")));
		btnNewButton_1.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNewButton_1.setBorder(null);
		btnNewButton_1.setBackground(new Color(43, 45, 49));
		btnNewButton_1.setBounds(10, 76, 50, 23);
		contentPane.add(btnNewButton_1);
		
		JScrollPane scrollPane = new JScrollPane();
		table = new JTable();
		scrollPane.setViewportView(table);
		ta();
		scrollPane.setBounds(10, 507, 815, 88);
		contentPane.add(scrollPane);
	}
}
