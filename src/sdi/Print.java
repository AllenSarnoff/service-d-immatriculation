package sdi;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.mysql.jdbc.Statement;


public class Print extends JFrame {

	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField numero;
	private JTextField nom;
	private JTextField adresse;
	private JTextField profession;
	private JTextField sexe;
	private JTextField marque;
	private JTextField date;
	private JTable table;
	private JScrollPane scrollPane;
	
	public void ta(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/CAR_REGISTER_ANTANANARIVO", "root", "");
			Statement stt = (Statement) conn.createStatement();
			ResultSet rs = stt.executeQuery("SELECT * FROM certificate ORDER BY register_date DESC");
			DefaultTableModel dtbm = (DefaultTableModel) table.getModel();
			
			String[] colName = {"Num�ro d'immatriculation", "Propri�taire","Adresse", "Profession","Sexe", "Marque", "Date d'immatriculation"};
				dtbm.setColumnIdentifiers(colName); 
				
			while(rs.next())
				{
					String num_car, name,adresse,profession,sexe, mark, register_date;
					num_car = rs.getString(1); 
					name = rs.getString(2);
					adresse = rs.getString(3);
					profession = rs.getString(4);
					sexe = rs.getString(5);
					mark = rs.getString(6); 
					register_date = rs.getString(7);
					String[] row = {num_car, name, adresse,profession ,sexe ,mark, register_date};
					dtbm.addRow(row);
				}
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Print frame = new Print();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Print() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(851, 719);
		setLocationRelativeTo(null);
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icone.png")));
		contentPane = new JPanel();
		contentPane.setBackground(new Color(43, 45, 49));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(242, 83, 91));
		panel.setBounds(10, 0, 815, 65);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("CAR REGISTER");
		lblNewLabel.setFont(new Font("Segoe Script", Font.BOLD, 40));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBackground(new Color(0, 0, 255));
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(217, 217, 217));
		panel_1.setBounds(10, 119, 601, 385);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_2 = new JLabel("1.");
		lblNewLabel_2.setForeground(new Color(77, 77, 77));
		lblNewLabel_2.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel_2.setBounds(10, 186, 19, 14);
		panel_1.add(lblNewLabel_2);
		
		numero = new JTextField();
		numero.setForeground(new Color(0, 0, 0));
		numero.setBorder(null);
		numero.setEditable(false);
		numero.setBackground(new Color(217, 217, 217));
		numero.setFont(new Font("Arial", Font.BOLD, 16));
		numero.setBounds(31, 185, 447, 20);
		panel_1.add(numero);
		numero.setColumns(10);
		
		JLabel lblNewLabel_2_1 = new JLabel("2.");
		lblNewLabel_2_1.setForeground(new Color(77, 77, 77));
		lblNewLabel_2_1.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel_2_1.setBounds(10, 212, 19, 15);
		panel_1.add(lblNewLabel_2_1);
		
		nom = new JTextField();
		nom.setForeground(new Color(0, 0, 0));
		nom.setBorder(null);
		nom.setEditable(false);
		nom.setBackground(new Color(217, 217, 217));
		nom.setFont(new Font("Arial", Font.BOLD, 16));
		nom.setColumns(10);
		nom.setBounds(31, 211, 447, 20);
		panel_1.add(nom);
		
		JLabel lblNewLabel_2_2 = new JLabel("3.");
		lblNewLabel_2_2.setForeground(new Color(77, 77, 77));
		lblNewLabel_2_2.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel_2_2.setBounds(10, 238, 19, 20);
		panel_1.add(lblNewLabel_2_2);
		
		adresse = new JTextField();
		adresse.setForeground(new Color(0, 0, 0));
		adresse.setBorder(null);
		adresse.setEditable(false);
		adresse.setBackground(new Color(217, 217, 217));
		adresse.setFont(new Font("Arial", Font.BOLD, 16));
		adresse.setColumns(10);
		adresse.setBounds(31, 240, 447, 20);
		panel_1.add(adresse);
		
		JLabel lblNewLabel_2_3 = new JLabel("4.");
		lblNewLabel_2_3.setForeground(new Color(77, 77, 77));
		lblNewLabel_2_3.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel_2_3.setBounds(10, 265, 19, 20);
		panel_1.add(lblNewLabel_2_3);
		
		profession = new JTextField();
		profession.setForeground(new Color(0, 0, 0));
		profession.setBorder(null);
		profession.setEditable(false);
		profession.setBackground(new Color(217, 217, 217));
		profession.setFont(new Font("Arial", Font.BOLD, 16));
		profession.setColumns(10);
		profession.setBounds(31, 267, 447, 20);
		panel_1.add(profession);
		
		JLabel lblNewLabel_2_4 = new JLabel("5.");
		lblNewLabel_2_4.setForeground(new Color(77, 77, 77));
		lblNewLabel_2_4.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel_2_4.setBounds(10, 295, 19, 20);
		panel_1.add(lblNewLabel_2_4);
		
		sexe = new JTextField();
		sexe.setForeground(new Color(0, 0, 0));
		sexe.setBorder(null);
		sexe.setEditable(false);
		sexe.setBackground(new Color(217, 217, 217));
		sexe.setFont(new Font("Arial", Font.BOLD, 16));
		sexe.setColumns(10);
		sexe.setBounds(31, 297, 447, 20);
		panel_1.add(sexe);
		
		JLabel lblNewLabel_2_4_1 = new JLabel("6.");
		lblNewLabel_2_4_1.setForeground(new Color(77, 77, 77));
		lblNewLabel_2_4_1.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel_2_4_1.setBounds(10, 330, 19, 15);
		panel_1.add(lblNewLabel_2_4_1);
		
		marque = new JTextField();
		marque.setForeground(new Color(0, 0, 0));
		marque.setBorder(null);
		marque.setEditable(false);
		marque.setBackground(new Color(217, 217, 217));
		marque.setFont(new Font("Arial", Font.BOLD, 16));
		marque.setColumns(10);
		marque.setBounds(31, 328, 447, 23);
		panel_1.add(marque);
		
		JLabel lblNewLabel_2_4_2 = new JLabel("7.");
		lblNewLabel_2_4_2.setForeground(new Color(77, 77, 77));
		lblNewLabel_2_4_2.setFont(new Font("Arial", Font.PLAIN, 20));
		lblNewLabel_2_4_2.setBounds(10, 358, 19, 14);
		panel_1.add(lblNewLabel_2_4_2);
		
		date = new JTextField();
		date.setForeground(new Color(0, 0, 0));
		date.setBorder(null);
		date.setEditable(false);
		date.setBackground(new Color(217, 217, 217));
		date.setFont(new Font("Arial", Font.BOLD, 16));
		date.setColumns(10);
		date.setBounds(31, 357, 447, 20);
		panel_1.add(date);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Print.class.getResource("/images/logo.png")));
		lblNewLabel_1.setBounds(0, 11, 601, 173);
		panel_1.add(lblNewLabel_1);
		
		JButton btnNewButton_2 = new JButton("Imprimer");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(numero.getText().equals("") || nom.getText().equals("") || adresse.getText().equals("") || profession.getText().equals("") || sexe.getText().equals("") || 
						marque.getText().equals("") || date.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Veuillez selectionner une ligne");				
				}
				else
				{
					
					File folder = new File("documents");
					if(!folder.exists()) {
						folder.mkdir();
					}
					
					String fileName = "documents/Carte-grise-"+numero.getText()+".pdf";
					
					Document doc = new Document();
					
					try {
						PdfWriter.getInstance(doc, new FileOutputStream(fileName));
						doc.open();
						
						Image img = Image.getInstance("logo.png");
						img.scaleAbsolute(523,150);
						img.setAlignment(Element.ALIGN_LEFT);
						doc.add(img);
						doc.add(new Paragraph("1. "+numero.getText().toUpperCase()));
						doc.add(new Paragraph("2. "+nom.getText().toUpperCase()));
						doc.add(new Paragraph("3. "+adresse.getText().toUpperCase()));
						doc.add(new Paragraph("4. "+profession.getText().toUpperCase()));
						doc.add(new Paragraph("5. "+sexe.getText().toUpperCase()));
						doc.add(new Paragraph("6. "+marque.getText().toUpperCase()));
						doc.add(new Paragraph("7. "+date.getText().toUpperCase()));
	
						doc.close();
						Desktop.getDesktop().open(new File(fileName));
						
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (DocumentException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (MalformedURLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
			}
		});
		
		btnNewButton_2.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_2.setHorizontalTextPosition(SwingConstants.LEADING);
		btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 30));
		btnNewButton_2.setForeground(new Color(242, 83, 91));
		btnNewButton_2.setIcon(new ImageIcon(Print.class.getResource("/images/print.png")));
		btnNewButton_2.setBorder(null);
		btnNewButton_2.setBackground(new Color(43, 45, 49));
		btnNewButton_2.setBounds(634, 468, 188, 43);
		contentPane.add(btnNewButton_2);
		
		scrollPane = new JScrollPane();
		table = new JTable();
		scrollPane.setViewportView(table);
		ta();
		DefaultTableModel dtb = (DefaultTableModel) table.getModel();
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int k=table.getSelectedRow();
				numero.setText(dtb.getValueAt(k, 0).toString().toUpperCase());
				nom.setText(dtb.getValueAt(k, 1).toString().toUpperCase());
				adresse.setText(dtb.getValueAt(k, 2).toString().toUpperCase());
				profession.setText(dtb.getValueAt(k, 3).toString().toUpperCase());
				sexe.setText(dtb.getValueAt(k, 4).toString().toUpperCase());
				marque.setText(dtb.getValueAt(k, 5).toString().toUpperCase());
				date.setText(dtb.getValueAt(k, 6).toString().toUpperCase());
			}
		});
		scrollPane.setBounds(10, 582, 815, 88);
		contentPane.add(scrollPane);
		
		JLabel lblNewLabel_3 = new JLabel("Aper\u00E7u : ");
		lblNewLabel_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_3.setBounds(10, 85, 80, 23);
		contentPane.add(lblNewLabel_3);
	}
}
