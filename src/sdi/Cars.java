package sdi;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
//import java.awt.event.KeyAdapter;
//import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class Cars extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private JScrollPane scrollPane;
	private JTextField num;
	private JTextField marque;
	private JTextField couleur;
	private JTextField use;
	private JTextField date_imm;

	public void ta(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/CAR_REGISTER_ANTANANARIVO", "root", "");
			Statement stt = (Statement) conn.createStatement();
			ResultSet rs = stt.executeQuery("SELECT * FROM cars");
			DefaultTableModel dtbm = (DefaultTableModel) table.getModel();
			
			String[] colName = {"Num�ro d'immatriculation", "Marque", "couleur", "usage", "Date d'immatriculation"};
				dtbm.setColumnIdentifiers(colName); 
				
			while(rs.next())
				{
					String num_car, brand, color, usage, register_date;
					num_car = rs.getString(1); 
					brand = rs.getString(2); 
					color = rs.getString(3); 
					usage = rs.getString(4); 
					register_date = rs.getString(5); 
					String[] row = {num_car ,brand ,color ,usage, register_date};
					dtbm.addRow(row);
				}
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cars frame = new Cars();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Cars() {
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(900, 700);
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icone.png")));
		contentPane = new JPanel();
		contentPane.setBackground(new Color(43, 45, 49));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(242, 83, 91));
		panel.setBounds(10, 0, 864, 65);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("CAR REGISTER");
		lblNewLabel.setFont(new Font("Segoe Script", Font.BOLD, 40));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBackground(new Color(0, 0, 255));
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Num\u00E9ro d'immatriculation");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(20, 112, 188, 14);
		contentPane.add(lblNewLabel_1);
		
		num = new JTextField();
		num.setEditable(false);
		num.setBounds(225, 110, 199, 20);
		contentPane.add(num);
		num.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Marque");
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_2.setBounds(20, 148, 188, 14);
		contentPane.add(lblNewLabel_2);
		
		marque = new JTextField();
		marque.setColumns(10);
		marque.setBounds(225, 146, 199, 20);
		contentPane.add(marque);
		
		JLabel lblNewLabel_3 = new JLabel("Couleur");
		lblNewLabel_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_3.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_3.setBounds(20, 188, 188, 14);
		contentPane.add(lblNewLabel_3);
		
		couleur = new JTextField();
		couleur.setColumns(10);
		couleur.setBounds(225, 186, 199, 20);
		contentPane.add(couleur);
		
		JLabel lblNewLabel_4 = new JLabel("Usage");
		lblNewLabel_4.setForeground(new Color(255, 255, 255));
		lblNewLabel_4.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_4.setBounds(20, 223, 188, 14);
		contentPane.add(lblNewLabel_4);
		
		use = new JTextField();
		use.setColumns(10);
		use.setBounds(225, 221, 199, 20);
		contentPane.add(use);
		
		JLabel lblNewLabel_5 = new JLabel("Date d'immatriculation");
		lblNewLabel_5.setForeground(new Color(255, 255, 255));
		lblNewLabel_5.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		lblNewLabel_5.setBounds(20, 262, 188, 14);
		contentPane.add(lblNewLabel_5);
		
		date_imm = new JTextField();
		date_imm.setColumns(10);
		date_imm.setBounds(225, 260, 199, 20);
		contentPane.add(date_imm);
		
		JButton btnNewButton = new JButton("Modifier");
		btnNewButton.setBorder(null);
		btnNewButton.setHorizontalTextPosition(SwingConstants.LEADING);
		btnNewButton.setIcon(new ImageIcon(Cars.class.getResource("/images/edit.png")));
		btnNewButton.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton.setForeground(new Color(242, 83, 91));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(num.getText().equals("") || marque.getText().equals("") || couleur.getText().equals("") || use.getText().equals("") || date_imm.getText().equals(""))
				{
					JOptionPane.showMessageDialog(null, "Veuillez selectionner une ligne \nEt assurez-vous de remplir tous les champs");
				}
				else
				{
					Connection conn ;
					try {
						conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");						
						PreparedStatement ps = (PreparedStatement) conn.prepareStatement("UPDATE cars SET mark=?, color=?, utilization=?, register_date=? WHERE number=?");
						ps.setString(1, marque.getText());
						ps.setString(2, couleur.getText());
						ps.setString(3, use.getText());
						ps.setString(4, date_imm.getText());
						ps.setString(5, num.getText());
						ps.executeUpdate();
						
						PreparedStatement ps1 = (PreparedStatement) conn.prepareStatement("UPDATE certificate SET mark=?, register_date=? WHERE number=?");
						ps1.setString(1, marque.getText());
						ps1.setString(2, date_imm.getText());
						ps1.setString(3, num.getText());						
						ps1.executeUpdate();

						Cars.this.dispose();
						
						Updated upd = new Updated();
				        upd.setVisible(true);
						
						
						
						CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS)
					    .execute(() -> {
									
					    	Cars ca = new Cars(); 
					    	ca.setVisible(true);
					    	ca.setLocationRelativeTo(null);
									 
					    });
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			}
		});
		btnNewButton.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		btnNewButton.setBackground(new Color(43, 45, 49));
		btnNewButton.setBounds(10, 303, 99, 32);
		contentPane.add(btnNewButton);
		
		
		
		JButton btnSupprimer = new JButton("Supprimer");
		btnSupprimer.setBorder(null);
		btnSupprimer.setHorizontalTextPosition(SwingConstants.LEADING);
		btnSupprimer.setIcon(new ImageIcon(Cars.class.getResource("/images/delete.png")));
		btnSupprimer.setHorizontalAlignment(SwingConstants.LEFT);
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(num.getText().equals("") ||  marque.getText().equals("") || couleur.getText().equals("") || use.getText().equals("") || date_imm.getText().equals("") )
				{
					JOptionPane.showMessageDialog(null, "Veuillez selectionner une ligne");
				}
				else {					
						Connection conn;
					try {
						conn = DriverManager.getConnection("jdbc:mysql://localhost/CAR_REGISTER_ANTANANARIVO", "root", "");
						PreparedStatement ps = (PreparedStatement) conn.prepareStatement("DELETE FROM cars WHERE number=?");
						ps.setString(1, num.getText());
						ps.executeUpdate();
						PreparedStatement ps1 = (PreparedStatement) conn.prepareStatement("DELETE FROM certificate WHERE number=?");
						ps1.setString(1, num.getText());
						ps1.executeUpdate();
						
	
						Cars.this.dispose();

						Deleted dlt = new Deleted();
						dlt.setVisible(true);
						
												
						CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS)
					    .execute(() -> {
									
					    	Cars ca = new Cars(); 
					    	ca.setVisible(true);
					    	
					    });
										
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				}
			}
		});
		btnSupprimer.setForeground(new Color(242, 83, 91));
		btnSupprimer.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		btnSupprimer.setBackground(new Color(43, 45, 49));
		btnSupprimer.setBounds(319, 303, 111, 32);
		contentPane.add(btnSupprimer);
		
		scrollPane = new JScrollPane();
		table = new JTable();
		scrollPane.setViewportView(table);
		ta();
		DefaultTableModel dtb = (DefaultTableModel) table.getModel();
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int k=table.getSelectedRow();
				num.setText(dtb.getValueAt(k, 0).toString());
				marque.setText(dtb.getValueAt(k, 1).toString());
				couleur.setText(dtb.getValueAt(k, 2).toString());
				use.setText(dtb.getValueAt(k, 3).toString());
				date_imm.setText(dtb.getValueAt(k, 4).toString());
			}
		});
		scrollPane.setBounds(10, 390, 864, 260);
		contentPane.add(scrollPane);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cars.this.dispose();
				Administrator admin = new Administrator();
				admin.setVisible(true);
			}
		});
		btnNewButton_1.setIcon(new ImageIcon(Cars.class.getResource("/images/previous.png")));
		btnNewButton_1.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNewButton_1.setBorder(null);
		btnNewButton_1.setBackground(new Color(43, 45, 49));
		btnNewButton_1.setBounds(10, 76, 50, 23);
		contentPane.add(btnNewButton_1);
		
	}
}
