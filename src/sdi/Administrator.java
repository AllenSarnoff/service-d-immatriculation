package sdi;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JSeparator;

public class Administrator extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Administrator frame = new Administrator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Administrator() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(809, 489);
		setLocationRelativeTo(null);        
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icone.png")));
		contentPane = new JPanel();
		contentPane.setBackground(new Color(43, 45, 49));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(242, 83, 91));
		panel.setBounds(10, 0, 773, 65);
		contentPane.add(panel);
		
		JLabel lblNewLabel = new JLabel("CAR REGISTER");
		lblNewLabel.setFont(new Font("Segoe Script", Font.BOLD, 40));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBackground(new Color(0, 0, 255));
		panel.add(lblNewLabel);
		
		JButton button1 = new JButton("Liste des admin");
		button1.setBackground(new Color(60, 62, 70));
		button1.setBorderPainted(false);
		button1.setForeground(new Color(255, 255, 255));
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator.this.dispose();
				TableAdmin admi = new TableAdmin();
				admi.setVisible(true);
			}
		});
		button1.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 15));
		button1.setBounds(10, 75, 186, 35);
		contentPane.add(button1);
		
		JButton button2 = new JButton("Liste des voitures");
		button2.setBackground(new Color(60, 62, 70));
		button2.setBorderPainted(false);
		button2.setForeground(new Color(255, 255, 255));
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator.this.dispose();
				Cars ca = new Cars();
				ca.setVisible(true);
			}
		});
		button2.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 15));
		button2.setBounds(202, 75, 186, 35);
		contentPane.add(button2);
		
		JButton button3 = new JButton("Liste des personnes");
		button3.setBackground(new Color(60, 62, 70));
		button3.setBorderPainted(false);
		button3.setForeground(new Color(255, 255, 255));
		button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator.this.dispose();
				Owners owner = new Owners();
				owner.setVisible(true);
			}
		});
		button3.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 15));
		button3.setBounds(394, 75, 186, 35);
		contentPane.add(button3);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Administrator.class.getResource("/images/t\u00E9l\u00E9chargement.png")));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(110, 145, 213, 211);
		contentPane.add(lblNewLabel_1);
		
		JLabel nom = new JLabel();
		nom.setForeground(new Color(242, 242, 242));
		nom.setText("Administrateur");
		nom.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 13));
		nom.setHorizontalAlignment(SwingConstants.CENTER);
		nom.setBounds(110, 367, 213, 22);
		contentPane.add(nom);
		
		JButton btnNewButton = new JButton("Ajouter un administrateur");
		btnNewButton.setBackground(new Color(60, 62, 70));
		btnNewButton.setBorderPainted(false);
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator.this.dispose();
				AddAdmin add = new AddAdmin();
				add.setVisible(true);
				add.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				
				
			}
		});
		btnNewButton.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 15));
		btnNewButton.setBounds(439, 183, 250, 30);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Enregistrer une voiture\r\n");
		btnNewButton_1.setBackground(new Color(60, 62, 70));
		btnNewButton_1.setBorderPainted(false);
		btnNewButton_1.setForeground(new Color(255, 255, 255));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator.this.dispose();
				CarRegister CR = new CarRegister();
				CR.setVisible(true);
				
			}
		});
		btnNewButton_1.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 15));
		btnNewButton_1.setBounds(439, 253, 250, 30);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_3 = new JButton("D\u00E9connection ");
		btnNewButton_3.setHorizontalTextPosition(SwingConstants.LEADING);
		btnNewButton_3.setIcon(new ImageIcon(Administrator.class.getResource("/images/logout.png")));
		btnNewButton_3.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_3.setBorder(null);
		btnNewButton_3.setBackground(new Color(43, 45, 49));
		btnNewButton_3.setForeground(new Color(242, 83, 91));
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator.this.dispose();
				Home log = new Home();
				log.setVisible(true);
			}
		});
		btnNewButton_3.setFont(new Font("Lucida Calligraphy", Font.BOLD, 18));
		btnNewButton_3.setBounds(601, 410, 181, 32);
		contentPane.add(btnNewButton_3);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(new Color(255, 255, 255));
		separator.setBounds(10, 63, 773, 7);
		contentPane.add(separator);
		
		JButton btnNewButton_1_1 = new JButton("Carte grise");
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Print pr = new Print();
				pr.setVisible(true);
			}
		});
		btnNewButton_1_1.setForeground(Color.WHITE);
		btnNewButton_1_1.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 15));
		btnNewButton_1_1.setBorderPainted(false);
		btnNewButton_1_1.setBackground(new Color(60, 62, 70));
		btnNewButton_1_1.setBounds(439, 326, 250, 30);
		contentPane.add(btnNewButton_1_1);
		
		
	
	}
}
