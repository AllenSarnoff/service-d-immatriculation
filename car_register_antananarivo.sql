-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : sam. 06 mai 2023 à 21:19
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `car_register_antananarivo`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `name` varchar(50) NOT NULL,
  `situation` varchar(50) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`username`, `password`, `name`, `situation`) VALUES
('allen', 'fe648522f967ab64ca9b0563b07dbab1d4afe5d2', 'Allen Austill', 'Visiteur temporaire'),
('fetra', '8feeb71c3b44f7b81d824bbc038453fdd4943486', 'RAMAHANDRISOA Fetra', 'Prof'),
('sarnoff', 'c50a392c4ba467984e9c397de2a9789bed127b45', 'TSARAVALY Sarnoff', 'Administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `cars`
--

DROP TABLE IF EXISTS `cars`;
CREATE TABLE IF NOT EXISTS `cars` (
  `number` varchar(20) NOT NULL,
  `mark` varchar(20) NOT NULL,
  `color` varchar(20) NOT NULL,
  `utilization` varchar(40) NOT NULL,
  `register_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cars`
--

INSERT INTO `cars` (`number`, `mark`, `color`, `utilization`, `register_date`) VALUES
('1212 TC', 'Nissan', 'Noire', 'Personnelle', '2023-02-15'),
('5892 TAA', 'Toyota', 'Blanche', 'Militaire', '2012-04-20'),
('9903 TBS', 'Renault', 'Grise', 'Transport personnel', '2022-06-15'),
('5265 TU', 'Volkswagen', 'Bleue', 'Militaire', '2023-06-02'),
('Z 4878', 'Toyota', 'Blanche', 'Armée', '2023-02-15'),
('Z 4878', 'Toyota', 'Blanche', 'Armée', '2023-02-15'),
('201 TAF 598', 'Land Cruiser', 'Marron', 'Banque mondiale', '2015-11-22'),
('205 TD 154', 'Suzuki', 'Grise', 'OMS', '2015-04-19');

-- --------------------------------------------------------

--
-- Structure de la table `certificate`
--

DROP TABLE IF EXISTS `certificate`;
CREATE TABLE IF NOT EXISTS `certificate` (
  `number` varchar(20) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `address` varchar(40) NOT NULL,
  `job` varchar(40) NOT NULL,
  `sex` varchar(5) NOT NULL,
  `mark` varchar(20) NOT NULL,
  `register_date` date NOT NULL,
  UNIQUE KEY `number` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `certificate`
--

INSERT INTO `certificate` (`number`, `full_name`, `address`, `job`, `sex`, `mark`, `register_date`) VALUES
('1212 TC', 'Kanto Annica', 'Fenoarivo', 'Etudiante', 'Femme', 'Nissan', '2023-02-15'),
('201 TAF 598', 'Casini Danaus', 'Antanifotsy', 'Médecin', 'Homme', 'Land Cruiser', '2015-11-22'),
('205 TD 154', 'Trilia Winner', 'Tanjombato', 'Ambassadrice', 'Femme', 'Suzuki', '2015-04-19'),
('5265 TU', 'Randriamaherisoa Alain', 'Antanimena', 'Général', 'Homme', 'Volkswagen', '2023-06-02'),
('5892 TAA', 'Niantsa Fehizoroniaina', 'Ambohibao', 'Etudiante', 'Femme', 'Toyota', '2012-04-20'),
('9903 TBS', 'Solofompampionona', 'Ambohipo', 'Comptable', 'Homme', 'Renault', '2022-06-15'),
('Z 4878', 'Ravalomanana Richard', 'Ambatobe', 'Jeneral', 'Homme', 'Toyota', '2023-02-15');

-- --------------------------------------------------------

--
-- Structure de la table `owners`
--

DROP TABLE IF EXISTS `owners`;
CREATE TABLE IF NOT EXISTS `owners` (
  `full_name` varchar(50) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `CIN` bigint(12) NOT NULL,
  `address` varchar(40) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `job` varchar(40) NOT NULL,
  `job_location` varchar(40) NOT NULL,
  PRIMARY KEY (`CIN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `owners`
--

INSERT INTO `owners` (`full_name`, `sex`, `CIN`, `address`, `phone`, `job`, `job_location`) VALUES
('Trilia Winner', 'Femme', 201457963, 'Tanjombato', '32 4578 964', 'Ambassadrice', 'Ampefiloha'),
('Ravalomanana Richard', 'Homme', 2104578896, 'Ambatobe', '32 45 789 63', 'Jeneral', 'Anosy'),
('Solofompampionona', 'Homme', 113254879623, 'Ambohipo', '345877962', 'Comptable', 'Andraharo'),
('Kanto Annica', 'Femme', 208659785541, 'Fenoarivo', '32 55 489 56', 'Etudiante', 'CUR'),
('Randriamaherisoa Alain', 'Homme', 213044578963, 'Antanimena', '334578412', 'Général', 'Analakely'),
('Casini Danaus', 'Homme', 215458987753, 'Antanifotsy', '34 01 425 50', 'Médecin', 'Befelatanana'),
('Niantsa Fehizoroniaina', 'Femme', 224587996310, 'Ambohibao', '34 58 798 62', 'Etudiante', 'CUR');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
